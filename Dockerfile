FROM python:3.6-alpine

LABEL maintainer "anders.harrisson@ess.eu"

RUN adduser -S awx

COPY esss-ca01-ca.crt ess-primary-ca-root.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates
ENV REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt

RUN apk add --no-cache libxml2-utils
COPY get_pom_version /usr/bin

ENV AWXKIT_VERSION 14.1.0
RUN pip install awxkit==$AWXKIT_VERSION

USER awx
