# awxkit
=========

[Docker] image for [awxkit] including the [AWX CLI], a command line tool for Ansible AWX_.

The image also includes xmllint and a simple script `get_pom_version` to get the version from the current pom.xml file.

## How to use this image

`docker run --rm registry.esss.lu.se/ics-docker/awxkit awx --help`

[docker]: https://www.docker.com
[awxkit]: https://github.com/ansible/awx/tree/devel/awxkit/
[AWX CLI]: https://docs.ansible.com/ansible-tower/latest/html/towercli/
